import React from 'react'
import {View,} from 'react-native';
import { connect } from "react-redux";
import Notification from '../components/Notification';

class NotificationWrapper extends React.Component {
    render() {
        const {children, user={}}=this.props;
        return (
            <View>
                {children}
                {//user.id &&
                <Notification/>
                }
            </View>
        );
    }
}

export default connect((store, props)=> ({
    user: store.userReducer.user
}))(NotificationWrapper);