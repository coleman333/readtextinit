import React from 'react';
import { createStackNavigator,} from 'react-navigation';
import MainMenu from "./MainMenu";
import BadInstagramCloneApp from './BadInstagramCloneApp';
import EditingPage from './EditingPage';
import {Button} from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import { Text, View } from 'react-native';


const Navigator = createStackNavigator({
    // QrScanner: { screen: QrScanner,
    //     navigationOptions:({ navigation }) =>({
    //         headerLeft:null,
    //         header: null,
    //         headerRight:(
    //             <Button style={{marginRight:10}}
    //                 onPress={() =>navigation.navigate('menuToScanGood')}
    //                 title="Info"
    //                 //color="#fff"
    //             />
    //         )
    //     })
    // },

    // MenuToScanGood: {screen: MenuToScanGood,
    //     navigationOptions:({navigation})=>({
    //         header:null
    //     })
    // },
    BadInstagramCloneApp: {screen: BadInstagramCloneApp,
        navigationOptions:({navigation})=>({
            header:null
        })
    },
    EditingPage: {screen: EditingPage,
        navigationOptions:({navigation})=>({
            header:null
        })
    },


    MainMenu: {screen: MainMenu,
        navigationOptions:({ navigation }) =>({
            headerLeft: null,
            header: null
            //     title: 'Home',
            //     headerStyle: {
            //         backgroundColor: 'gray',
            //
            //     },
            //     headerTintColor: '#fff',
            //     headerTitleStyle: {
            //         marginLeft:'45%',
            //         // display:'flex',
            //         // flex:1,
            //         // flexDirection:'row',
            //         // justifyContent:'center',
            //         // alignItems:'center',
            //         fontWeight: 'bold',
            //     },
        })
    },

}, {initialRouteName: 'MainMenu'});

export default Navigator;
