'use strict';
import React, {Component} from 'react';
import {
    ActivityIndicator,
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import userAction from "../actions/userAction";
// import MainMenu from "../containers/MainMenu";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import EditingPage from './EditingPage';

const styles2 = StyleSheet.create({
    container: {
        // flex: 1,
        justifyContent: 'center',
        position:'absolute',
        alignItems: 'center'
    },

})

class BadInstagramCloneApp extends Component {

    componentDidMount(){
        // if(this.props.text){
        //     let { navigate } = this.props.navigation;
        //     navigate('MainMenu');
        // }
    }

    render() {
        const {amountOfActiveProcesses: processing} = this.props;
// alert(`amount of processes: ${processing}`)
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.off}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                    onGoogleVisionBarcodesDetected={({barcodes}) => {
                        alert(barcodes)
                    }}
                    onFacesDetected={({face}) => {
                        alert(face)
                    }}
                >

                </RNCamera>
                <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center',}}>
                    <TouchableOpacity
                        onPress={this.takePicture.bind(this)}
                        style={styles.capture}
                    >
                        <Text style={{fontSize: 14}}> SNAP </Text>
                    </TouchableOpacity>
                </View>
                {
                    !!processing && <View style={[styles2.container]}>
                        <ActivityIndicator size="large" color="#0000ff"/>
                    </View>
                }
            </View>
        );
    }

    takePicture = async function () {
        if (this.camera) {
            const options = {quality: 1.0, base64: true, doNotSave: false};
            const data = await this.camera.takePictureAsync(options)
            // alert(data.base64);
            this.props.userAction.readText(data)
                .then(()=>{
                    // if(this.props.userReducer.text){
                        let { navigate } = this.props.navigation;
                        navigate('EditingPage');
                    // }
                })
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    }
});

const mapStateToProps = (state) => {
    // console.log(state.dimension);
    return {
        // users: state.userReducer.allUsers,
        // gists: state.userReducer.gists
        // user: state.userReducer.login,
        amountOfActiveProcesses: state.common.amountOfActiveProcesses,
        text: state.userReducer.readText
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BadInstagramCloneApp);