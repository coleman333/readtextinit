import React from 'react';
import {
    StyleSheet,
    Text,
    Button,
    View,
    TextInput,
    TouchableOpacity,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    Animated,
    Keyboard,
    Dimensions,
    Image,
    Easing,
    TouchableHighlight,
    TouchableWithoutFeedback,
} from 'react-native';

import {connect} from "react-redux";
import userAction from '../actions/userAction';
import {bindActionCreators} from 'redux';
import {FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import _ from 'lodash';
import NotificationWrapper from './NotificationWrapper';
import BadInstagramCloneApp from './BadInstagramCloneApp';

import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';


import {StackActions, NavigationActions} from 'react-navigation';

const IMAGE_HEIGHT = 408;
const IMAGE_HEIGHT_SMALL = 24;
import Cookie from 'react-native-cookie'


class EditingPage extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.y_translate = new Animated.Value(0);

    }


    componentWillMount() {
        this.animatedValue1 = new Animated.Value(1);

        if (this.props.navigation.state.params) {
            const {page} = this.props.navigation.state.params;
            if (page === "unAuthorized") {
                alert('Вы не зарегистрированы')
            }
        }
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        this.setState({launchAnimation: 'inActive'})

        // this.onChange=_.debounce(this.onChange.bind(this), 1000);
    }

    spinValue = new Animated.Value(0);
    _animValue2 = new Animated.Value(0);

    // spring = new Animated.Value(0);

    componentDidMount() {

        Animated.loop(
            Animated.timing(
                this.spinValue,
                {
                    toValue: 1,
                    duration: 3000,
                    // easing: Easing.linear,
                    useNativeDriver: true
                }
            )
        ).start();
        this.setState({text: this.props.text})
    }

    state = {
        toScroll: false,
        keyboardHeight: 0,
        normalHeight: 0,
        shortHeight: 0,
        heightOfElementWhenKeyboardOn: '100%',
        item: 1,
        // launchAnimation: 'active',
        facebookName: '',
        isntaToken: '',
        disAnimate: true,
        menu_expanded: false,
        text: ''
    };


    componentWillUnmount() {
        // this.keyboardDidShowListener.remove();
        // this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        //     this.setState({toScroll: true});
        //     this.setState({shortHeight: Dimensions.get('window').height - e.endCoordinates.height});
        //     this.setState({normalHeight: Dimensions.get('window').height});
        //     this.setState({keyboardHeight: e.endCoordinates.height});
        //     this.setState({heightOfElementWhenKeyboardOn: this.state.shortHeight});
    }

    _keyboardDidHide() {
        //     this.setState({toScroll: false});
        //     this.setState({heightOfElementWhenKeyboardOn: this.state.normalHeight});
        //
    }

    netsAuthorization() {
        Animated.timing(
            this._animValue2,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: false})
    }

    authPanelDisAnimate() {
        console.log('Test press');

        Animated.timing(
            this._animValue2,
            {
                toValue: 0,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: true})
    }

    handlePressIn() {
        Animated.spring(this.animatedValue1, {
            toValue: 0.5
        }).start()
    }

    handlePressOut() {
        Animated.spring(this.animatedValue1, {
            duration: 2000,
            toValue: 1,
            friction: 4,
            tension: 40
        }).start()
    }

    readText() {
        // this.props.userAction.readText() //because of it has no arguments passed through.
    }

    redirectToCamera() {
        let {navigate} = this.props.navigation;

        navigate('BadInstagramCloneApp');
    }

    render() {

        const {toScroll} = this.state;
        const inputAccessoryViewID = "uniqueID";

        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })

        const slide2 = this._animValue2.interpolate({
            inputRange: [0, 1],
            outputRange: [-500, 0],


        })
        const animatedStyle = {transform: [{scale: this.animatedValue1}]}
        return (
            //<View style={mainContainer(this.state.heightOfElementWhenKeyboardOn).container}>
            <NotificationWrapper>
                <KeyboardAvoidingView enabled behavior="padding">
                    <ScrollView
                        // style={mainContainer(this.state.heightOfElementWhenKeyboardOn).container}
                    >
                        {/*<TouchableWithoutFeedback onPressOut={this.authPanelDisAnimate.bind(this)}>*/}
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'gray',
                            height: "100%"
                        }}>
                            {/*<Input*/}
                            {/*placeholder='INPUT WITH SHAKING EFFECT'*/}
                            {/*shake={true}   multiline = {true}  numberOfLines = {4}*/}
                            {/*onChangeText={(text) => this.setState({text})}*/}
                            {/*value={this.state.text}*/}
                            {/*style={{ height: 600, backgroundColor: '#ccc' }}*/}
                            {/*/>*/}
                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                blurOnSubmit={false}
                                onChangeText={(text) => this.setState({text})}
                                value={this.state.text}
                            />
                            {/*<Text>{this.props.text}</Text>*/}
                            <View style={Container.container}>
                                <Animated.Image
                                    style={{transform: [{rotateY: spin}], width: 200, height: 200}}
                                    source={require('../images/SmartITLogo.png')}
                                />
                            </View>


                        </View>
                        {/*</TouchableWithoutFeedback>*/}
                    </ScrollView>
                </KeyboardAvoidingView>
            </NotificationWrapper>
        );
    }
}

const animButtonStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonStyle: {

        height: 50,
        // width:100,
        borderRadius: 25,
        backgroundColor: 'black',

    }
})

const st = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    label: {
        fontSize: 16,
        fontWeight: 'normal',
        marginBottom: 48,
    },
});

const Container = StyleSheet.create({
    container: {
        flex: 1,
        // display: 'flex',
        // width: "100%",
        // height: '100%',
        // backgroundColor: 'white',
        // overflow: 'scroll',
        alignItems: 'center',
        // justifyContent: 'space-around'
        justifyContent: 'center'
    },
});

const mapStateToProps = (state) => {
    console.log(state.dimension);
    return {
        // users: state.userReducer.allUsers,
        // gists: state.userReducer.gists
        // user: state.userReducer.login,
        text: state.userReducer.text

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditingPage);
