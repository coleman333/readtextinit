import { createActionAsync } from "redux-act-async";
import axios from "axios";
// import { createAction } from "redux-actions";
import _ from 'lodash';

export default {
    getTestUsers: createActionAsync('GET_TEST_USERS', () => {
       return axios({
            method: 'get',
            url: `https://jsonplaceholder.typicode.com/users`,
        });
    }, {rethrow: true}),

    login: createActionAsync('LOGIN', (user) => {
        return axios({
            method: 'post',
             url: `http://192.168.0.201:3003/api/users/login`,
             // url: `http://192.168.0.201:4433/api/users/login`,
            data:  {user},
        });
    }, {rethrow: true}),
    productScan: createActionAsync('PRODUCT', (product) => {
        return axios({
            method: 'get',
            url: `http://192.168.0.109:4433/api/users/product`,product
            // data: { user},
        });
    }, {rethrow: true}),
    readText: createActionAsync('READ_TEXT', ({base64:base64content, uri}) => {
        alert('some text here',uri);
        // let data = new FormData();
        // data.append('file',base64content);

        return axios({
            method: 'post',
            url: `http://192.168.0.201:3003/api/users/test-tesseract-ocr`,
            data:{fileBase64content:base64content, extension:_.last(_.split(uri,'.'))},
        });
    }, {rethrow: true}),

    sendPushNotification: createActionAsync('SEND_PUSH_NOTIFICATION', (message) => {
        console.log(message , 'this is action side');
        return axios({
            method: 'post',
            url: `http://192.168.0.201:3003/api/users/send-push-notifications`,
            data: { message},
        });
    }, {rethrow: true}),



};

